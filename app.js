const axios = require('axios');
const commandLineArgs = require('command-line-args')
const logger = require('js-logger')
var figlet = require('figlet');
const fs = require('fs');
logger.useDefaults()

const optionDefinitions = [
    { name: 'help', alias: 'h', type: Boolean },
    { name: 'email', alias: 'e', type: String },
    { name: 'user', alias: 'u', type: String }
]

const options = commandLineArgs(optionDefinitions)

let help = function() {
    figlet('sshresync', function(err, data) {
        if (err) {
            logger.error(err)
            return;
        }
        console.log(data)
        console.log(`Author - Kristopher Clark`)
        console.log(`Copyright (C) 2023`)
        console.log(`GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007\n`)
        console.log(`--help  -h  -- Prints the help screen`)
        console.log(`--email -e  -- Email associated with your Ubuntu One account.  Required`)
        console.log(`--user  -u  -- Which Ubuntu user's authorized_keys to update.  Required`)
    });
}

if(!options.email || !options.user){
    help();
}else{
    if(options.help) {
        help();
    }else{
        try {
            axios.get(`https://login.ubuntu.com/api/v2/keys/${options.email}`).then((response)=>{
                let keys = ""
                response.data.ssh_keys.forEach((key)=>{
                    keys += `${key}\n`
                })
                fs.writeFileSync(`/home/${options.user}/.ssh/authorized_keys`, `${keys}`);
            }).catch((exception)=>{
                logger.error(`Error Code: ${exception.cause.errno}\nCause code: ${exception.cause.code}\nSyscall: ${exception.cause.syscall}`);
            });
        }catch(exception){
            logger.error(exception)
        }
    }
}

