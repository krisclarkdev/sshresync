# sshresync

A packaged script written in [NodeJS](https://nodejs.org/en/) that will fetch all public [ssh keys](https://www.ssh.com/academy/ssh-keys) associated with the email address provided and update the [authorized_keys](https://www.ssh.com/academy/ssh/authorized-keys-file) file for the user provided.  This is intended to be run with a [cronjob](https://www.hostinger.com/tutorials/cron-job) to keep [Ubuntu Core](https://ubuntu.com/core) ssh keys up to date

## Disclaimer

I've not tested this on any of my production Ubuntu Core instances yet.  Run at your own risk.  If this doesn't work properly you run the risk of locking yourself out of your system.  It is up to you to make the necessary changes and due dilligence needed to prevent that.  I assume no responsibility for unexpected consequences.  I will update this disclaimer after having tested it on my production machines.

********If your key is not in your [Ubuntu One](https://login.ubuntu.com/) account when this runs it will not be present afterwards********

# Usage

Set a crobjob to run at your desired interval.  The following will fetch the public keys for the email example@acme.com and update the authorized_keys file for user "example"

```shell
* * * * * /path/to/sshresync --email example@acme.com --user example
```

# Building

Update the example to match your target system

This assumes you have nodejs and npm installed on your system.  I build this with NodeJS v18.0.2 and npm v9.2.0

```shell
git clone https://gitlab.com/krisclarkdev/sshresync.git
cd sshresync
npm install pkg -g
npm install
pkg app.js --platform linux --arch arm64
```

Then [scp](https://linuxize.com/post/how-to-use-scp-command-to-securely-transfer-files/) the binary to the target machine

# Prebuilt binaries

Coming soon
